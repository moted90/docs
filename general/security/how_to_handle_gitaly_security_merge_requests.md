## How to handle Gitaly Security merge requests?

Gitaly security issues are rare and not automatically embedded in the security
release process. There composed of two sections:

1. The Gitaly security fix prepared on the Gitaly security repository and,
2. The merge request that bumps the Gitaly version on GitLab, prepared on the GitLab security
repository.

The first part is prepared by the Gitaly team, the second part requires manual intervention
by Release Manager, this last part aims to be automated by
[performing the Gitaly update task during the Security Release].

Once the Gitaly security merge requests are ready:

1. Merge the security merge request targeting `master` on the Gitaly repository.
2. Disable the automatic [Gitaly update task].
3. Bump the `GITALY_VERSION` on the GitLab security repository by using the merge sha
from the Gitaly security merge request (see [example])
4. Prior to merging the backports, ensure the merge request that bumps the version has been
deployed to GitLab.com.

After the security release is published, re-enable the Gitaly update task.

[Gitaly security]: https://gitlab.com/gitlab-org/security/gitaly/
[GitLab security]: https://gitlab.com/gitlab-org/security/gitlab/
[performing the Gitaly update task during the Security Release]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1236
[example]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2176
[Gitaly update task]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/112/edit
