# Security mirrors

To create and configure a security mirror Owner access to the Security group
is required, for compliance purposes this is limited to Delivery team members. If you're
outside the Delivery team and require a Security mirror to be set up [file an issue] on
the Delivery tracker.

One of GitLab's core values is "[public by default]", which means we develop in
the open. One exception to this is security fixes, because developing
security fixes in public discloses vulnerabilities before a fix is available,
exposing ourselves and our users to attacks.

In order to work on these security issues in private, public GitLab projects
have a security mirror in the [`gitlab-org/security`] group.

Below we'll outline processes and considerations for creating these mirrors
and keeping them in sync with their public ("Canonical") counterparts.

[public by default]: https://about.gitlab.com/handbook/values/#public-by-default
[`gitlab-org/security`]: https://gitlab.com/gitlab-org/security/
[file an issue]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/new?issuable_template=mirroring_request

## Creating a Security mirror

A Security mirror:

* **must** be a fork of its Canonical project. Our release
  tooling relies on this fork relationship to verify the status of the
  mirroring configurations between all of a project's repositories.
* **should** have the same subgroup structure as its Canonical project, but
  within the `security` subgroup. For example, a project at
  `gitlab-org/build/images/example` should have a Security mirror at
  <code>gitlab-org/<b>security/</b>build/images/example</code>. This makes it
  easier to reason about where the Canonical project will be for a Security
  project, and vice-versa.
* **should** give a clear indication to GitLab team members that it's a
  Security project. Prior examples of this would be adding the lock icon
  (:lock:) as a prefix in the **project name** field, and modifying the
  project description (e.g., `:lock: Security mirror of gitlab-org/gitlab
  :lock:`).
* **should** require each merge request to receive at least one approval from
  a member of the `gitlab-com/gl-security/appsec` group.
* **should** disable project features it doesn't need, such as the Container
  registry, Forks, Wikis, and Snippets.

## Configuring mirroring

We rely on [push mirroring] from the Canonical project to update the Security project.

In the Canonical project:

1. Go to **Settings** > **Repository** > **Mirroring repositories**
1. Change **Mirror direction** to **Push**
1. Fill in **Git repository URL** with the _HTTPS URL_ to the Security
   repository, along with the `gitlab-bot` username (e.g.,
   `https://gitlab-bot@gitlab.com/gitlab-org/security/gitlab.git`)
1. In the 1Password Engineering vault, view the `gitlab.com - gitlab-bot` credentials.
1. Copy the **Merge Train and Security mirror token** password into the **Password** field.
1. Enable the **Keep divergent refs** and **Only mirror protected branches** options.

If the project also needs to be mirrored to Build (`dev.gitlab.org`) for its
release process, follow these steps to set up the repo in Dev and create a new mirror in the **Security** project:

1. Go to `dev.gitlab.org/gitlab` and click (top right) to create a **New project** 
1. Select `Import project` and set up using the Canonical repository url
1. Add the `gitlab_pushbot` as a  `Maintainer` on the project
1. In the **Security** project go to **Settings** > **Repository** > **Mirroring repositories**
1. Change **Mirror direction** to **Push**
1. Fill in the **Git repository URL** with the _HTTPS URL_ to the
   dev.gitlab.org repository, along with the `gitlab_pushbot` username (e.g.,
   `https://gitlab_pushbot@dev.gitlab.org/gitlab/gitlab-ee.git`)
1. In the 1Password Build vault, view the `gitlab_pushbot` credentials.
1. Copy the **Security to Build (dev) push mirroring** password into the **Password** field.
1. Enable the **Keep divergent refs** and **Only mirror protected branches** options.
1. For `omnibus-gitlab` to be able to access the repository over SSH, its deploy key needs to be enabled in the Build mirror
    1. Go to **Settings** > **Repository** > **Deploy keys**
    1. In the **Privately accessible deploy keys** tab, enable the key named `omnibus-builder deploy key`

**NOTE:**  
If the project has its releases managed by
[release-tools](https://gitlab.com/gitlab-org/release-tools), ensure that
`@gitlab-release-tools-bot` has Maintainer level access to all the mirrors of
the project. The recommended way to do this is to share the project with the
Delivery team GitLab group in both
[GitLab.com](https://gitlab.com/gitlab-org/delivery) and
[dev.gitlab.org](https://dev.gitlab.org/gitlab/delivery). The Release Tools Bot
is a member of this GitLab group and will automatically inherit the permissions.

[push mirroring]: https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pushing-to-a-remote-repository

## Related issues

* <https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/510>
